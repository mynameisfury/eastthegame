{
    "id": "85ac305a-d8cc-4c64-a962-b8ad786d6c2f",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_menu",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "MV Boli",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c82cdeb7-40fb-4d7a-940d-86ac01e25c97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 52,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 445,
                "y": 110
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "448c207d-2c20-4b95-9980-50d89255b603",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 52,
                "offset": 2,
                "shift": 12,
                "w": 11,
                "x": 257,
                "y": 164
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "cc273bfb-7c23-42dc-b610-0ca47dbf5a97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 52,
                "offset": 3,
                "shift": 15,
                "w": 14,
                "x": 137,
                "y": 164
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e2cfd433-e93d-4fac-b01e-4c18bdf8e7fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 52,
                "offset": 1,
                "shift": 22,
                "w": 22,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "3969c51d-e0d7-4d91-a10b-70b96a46aa96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 52,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 435,
                "y": 56
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ec31d2b1-e82f-4e25-91dc-fd98dc793f8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 52,
                "offset": 2,
                "shift": 22,
                "w": 23,
                "x": 384,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ec1ebe50-21f3-4538-bb61-e6f2baa666d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 52,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 258,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4b5cc8d2-cfb2-4646-bc65-0c0920c7d3b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 52,
                "offset": 3,
                "shift": 7,
                "w": 7,
                "x": 360,
                "y": 164
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4e1ca3b9-3e38-4b86-986e-336a781cdbb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 52,
                "offset": 3,
                "shift": 17,
                "w": 17,
                "x": 259,
                "y": 110
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "2c8c1cd7-c6c9-4840-883a-da3c59e85af8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 52,
                "offset": -1,
                "shift": 17,
                "w": 17,
                "x": 278,
                "y": 110
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c173c37a-5b93-45d5-beaf-5ad393d6746d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 52,
                "offset": 2,
                "shift": 12,
                "w": 13,
                "x": 200,
                "y": 164
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "380fd004-4027-467e-bff6-dd0ca932f920",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 52,
                "offset": 2,
                "shift": 18,
                "w": 17,
                "x": 297,
                "y": 110
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "636f1464-ee63-4ec0-8d4c-c519887ab014",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 52,
                "offset": 2,
                "shift": 12,
                "w": 6,
                "x": 369,
                "y": 164
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "93c91d22-4c18-4feb-ba1c-21b53814a12f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 52,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 243,
                "y": 164
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3319c361-e3c1-4861-a1d2-57c08db2081b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 52,
                "offset": 5,
                "shift": 12,
                "w": 5,
                "x": 377,
                "y": 164
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "03037034-93d8-4c4d-b969-e09cac598bcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 52,
                "offset": 0,
                "shift": 16,
                "w": 20,
                "x": 326,
                "y": 56
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "177dd344-b1c1-407f-b63a-345c8635eb5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 52,
                "offset": 2,
                "shift": 22,
                "w": 21,
                "x": 235,
                "y": 56
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b925ee78-7cff-4430-a719-2575569bd637",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 52,
                "offset": 3,
                "shift": 13,
                "w": 11,
                "x": 270,
                "y": 164
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "931eb64d-e12c-401b-a5b9-ea416264bae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 52,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 166,
                "y": 56
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ffd597d5-9d1b-4da7-818a-06ec87a6dbb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 52,
                "offset": 1,
                "shift": 21,
                "w": 21,
                "x": 258,
                "y": 56
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2785aaab-2e38-4743-aec7-e4a6ba177d39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 52,
                "offset": 2,
                "shift": 22,
                "w": 20,
                "x": 370,
                "y": 56
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f47991bd-c6c5-4eb9-8c6a-9ba7a75ea5b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 52,
                "offset": 1,
                "shift": 21,
                "w": 22,
                "x": 433,
                "y": 2
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "94d336bc-d2dd-4635-bdf3-f007abb85409",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 52,
                "offset": 2,
                "shift": 21,
                "w": 21,
                "x": 212,
                "y": 56
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "817abfeb-69b9-49d0-ba1e-c6f918ee5e15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 52,
                "offset": 4,
                "shift": 19,
                "w": 19,
                "x": 456,
                "y": 56
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8f8bee5f-1954-43b0-a854-7caa90f8772c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 52,
                "offset": 1,
                "shift": 21,
                "w": 23,
                "x": 284,
                "y": 2
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "26202679-0e0f-4669-a7e0-dd344834443d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 52,
                "offset": 3,
                "shift": 21,
                "w": 19,
                "x": 477,
                "y": 56
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "70bdd343-d819-44bc-881c-b1ec60906d72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 52,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 295,
                "y": 164
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "055d4ea4-5c16-483d-8c14-33c414d410d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 52,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 306,
                "y": 164
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "848601b1-8b8b-429e-a0e7-eaf9ff7dce86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 52,
                "offset": 2,
                "shift": 18,
                "w": 18,
                "x": 162,
                "y": 110
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "3c26e82f-b1e2-42e9-ae5b-4eae40c32aa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 52,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 481,
                "y": 110
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "8f3a01d2-d6dd-47fc-b479-cedb1cfdc63f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 52,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 221,
                "y": 110
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f84d4853-96ed-4b9f-8c4f-ff660991da4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 52,
                "offset": 4,
                "shift": 17,
                "w": 17,
                "x": 354,
                "y": 110
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "707ea711-8e81-4b79-9165-1c3f777fa737",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 52,
                "offset": 4,
                "shift": 28,
                "w": 27,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "5cf6886a-8070-4b48-b620-bab387c4f6b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 52,
                "offset": 2,
                "shift": 22,
                "w": 21,
                "x": 120,
                "y": 56
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "509843e2-68a8-4ba0-bb3f-501689f29401",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 52,
                "offset": 2,
                "shift": 19,
                "w": 21,
                "x": 97,
                "y": 56
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5b599554-e582-4f00-b3c8-154df7cb07f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 52,
                "offset": 3,
                "shift": 19,
                "w": 18,
                "x": 102,
                "y": 110
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fb487109-9714-4f7b-a2ef-833be9f711ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 52,
                "offset": 1,
                "shift": 22,
                "w": 23,
                "x": 334,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b3c2cf73-0364-41a5-b417-cb753e0ee8c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 52,
                "offset": 3,
                "shift": 19,
                "w": 21,
                "x": 74,
                "y": 56
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9998f8fa-5552-4926-ba98-a6d690decffd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 52,
                "offset": 3,
                "shift": 18,
                "w": 20,
                "x": 348,
                "y": 56
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1995bc0e-c961-4105-93a6-8e920f54c728",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 52,
                "offset": 3,
                "shift": 22,
                "w": 21,
                "x": 143,
                "y": 56
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "df44fe1b-08a8-42ba-ae2d-a204651f10c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 52,
                "offset": 2,
                "shift": 23,
                "w": 24,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "de0cbaed-02d2-479d-9d8e-e7614f13bc87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 52,
                "offset": 3,
                "shift": 9,
                "w": 9,
                "x": 317,
                "y": 164
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "cce55c63-ac95-4c9c-942a-fbcabe42e773",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 52,
                "offset": 1,
                "shift": 22,
                "w": 27,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d5478940-850d-4361-8ecb-6b2b91419801",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 52,
                "offset": 3,
                "shift": 22,
                "w": 20,
                "x": 392,
                "y": 56
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0cf3a9ce-648a-43a1-a450-eb08fe7e4cd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 52,
                "offset": 3,
                "shift": 19,
                "w": 18,
                "x": 122,
                "y": 110
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "8b5b5917-e10e-47d2-b1d5-cda70d915f42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 52,
                "offset": 2,
                "shift": 30,
                "w": 29,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "6b7ee1fc-027b-4cef-b89e-914b3c61d20b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 52,
                "offset": 2,
                "shift": 25,
                "w": 26,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "4a5971c7-f9ba-43c9-b6be-c85df98c90ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 52,
                "offset": 3,
                "shift": 23,
                "w": 22,
                "x": 457,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2c0281f5-5ab9-49f3-adbc-1045bee2f315",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 52,
                "offset": 3,
                "shift": 21,
                "w": 22,
                "x": 409,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "da8ae2c2-0624-40d4-a301-5539f7919607",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 52,
                "offset": 3,
                "shift": 22,
                "w": 22,
                "x": 26,
                "y": 56
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ace705ce-dbe0-4982-b800-ee07a320978b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 52,
                "offset": 3,
                "shift": 22,
                "w": 21,
                "x": 189,
                "y": 56
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b32c6f26-eb18-497a-9d9e-28b6d3845087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 52,
                "offset": 2,
                "shift": 20,
                "w": 21,
                "x": 281,
                "y": 56
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a0194c9d-579f-4c4d-842a-a73059dddb07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 52,
                "offset": 5,
                "shift": 21,
                "w": 23,
                "x": 309,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e4ec4193-1140-4faa-92b0-f44a6c729090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 52,
                "offset": 4,
                "shift": 23,
                "w": 22,
                "x": 50,
                "y": 56
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "10a96fa6-0ee4-425a-aee3-7acd9a6afc1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 52,
                "offset": 5,
                "shift": 21,
                "w": 20,
                "x": 304,
                "y": 56
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b5efa167-62cf-4738-ac7f-77dd778ce5b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 52,
                "offset": 3,
                "shift": 30,
                "w": 29,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "350d1e03-99d8-40c0-b2aa-1bc7fe17e548",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 52,
                "offset": 0,
                "shift": 23,
                "w": 25,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "6b3e5a23-7783-4c0e-af44-13e37452ecfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 52,
                "offset": 4,
                "shift": 22,
                "w": 23,
                "x": 359,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "2d161428-c635-449a-8b79-3457970eb796",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 52,
                "offset": 2,
                "shift": 26,
                "w": 26,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "9acc0b0c-d6a1-4ebf-81b3-91601425f7b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 52,
                "offset": 0,
                "shift": 14,
                "w": 18,
                "x": 42,
                "y": 110
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ecd64cd4-6ac0-4947-aef5-8dcdf9a08712",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 52,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 283,
                "y": 164
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "d959c584-3594-458f-b4ce-4c6f39849e48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 52,
                "offset": -2,
                "shift": 14,
                "w": 17,
                "x": 202,
                "y": 110
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6e7b4783-ba3f-4ec0-a20c-90a5a32b25aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 52,
                "offset": 3,
                "shift": 15,
                "w": 17,
                "x": 316,
                "y": 110
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a0553f86-e0ea-4571-9022-f52348558b36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 52,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 463,
                "y": 110
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d43c5cbb-d5d2-4142-ab7b-ea97933bad9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 52,
                "offset": 9,
                "shift": 17,
                "w": 8,
                "x": 350,
                "y": 164
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "04e54980-a3d3-419e-af31-51e3742014a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 52,
                "offset": 2,
                "shift": 16,
                "w": 15,
                "x": 2,
                "y": 164
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5ddd1718-c88f-4d10-ac30-8f39f55ae0f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 52,
                "offset": 3,
                "shift": 17,
                "w": 15,
                "x": 87,
                "y": 164
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "02470812-4f92-4293-b30e-8150544df1ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 52,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 229,
                "y": 164
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "8e3ca6d2-a25d-427b-899f-a0faf8def7a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 52,
                "offset": 2,
                "shift": 17,
                "w": 18,
                "x": 22,
                "y": 110
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7da5c5f8-43e2-482f-94e1-9e491b411dcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 52,
                "offset": 2,
                "shift": 16,
                "w": 15,
                "x": 104,
                "y": 164
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "cf55eaa4-407a-4681-8746-3d34ea64bb0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 52,
                "offset": 3,
                "shift": 13,
                "w": 18,
                "x": 62,
                "y": 110
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "29a34270-baf6-4e0c-a7ed-42d2da8e408e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 52,
                "offset": 2,
                "shift": 15,
                "w": 14,
                "x": 153,
                "y": 164
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a55aaaf5-d3e4-4e5b-b570-0cd485dcf701",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 52,
                "offset": 3,
                "shift": 18,
                "w": 15,
                "x": 70,
                "y": 164
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "823a2e1e-a0de-44d4-8c65-78c643f5a74a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 52,
                "offset": 2,
                "shift": 8,
                "w": 9,
                "x": 339,
                "y": 164
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7be44e4d-0c06-417f-9b3e-2e9e7ecdb5d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 52,
                "offset": -4,
                "shift": 8,
                "w": 14,
                "x": 121,
                "y": 164
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "6685e3e2-0d89-434d-8343-517e14972e73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 52,
                "offset": 2,
                "shift": 15,
                "w": 16,
                "x": 427,
                "y": 110
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "49ae15ab-7b37-4523-9820-7e40bca3badf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 52,
                "offset": 3,
                "shift": 8,
                "w": 9,
                "x": 328,
                "y": 164
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "beb7b94f-d2b8-495f-913c-bed644439847",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 52,
                "offset": 3,
                "shift": 27,
                "w": 25,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "983dfd30-66a0-4702-9f6b-cdbec2f92561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 52,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 373,
                "y": 110
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "533342c8-26bb-496b-93d2-fc5cc365f80f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 52,
                "offset": 2,
                "shift": 17,
                "w": 16,
                "x": 391,
                "y": 110
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b3bbb33c-0234-4133-813b-6719a0afe760",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 52,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "eb626266-3e0d-4ddb-9bf5-4d8c61e49c10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 52,
                "offset": 2,
                "shift": 17,
                "w": 16,
                "x": 409,
                "y": 110
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c158fa90-2cac-423a-9414-c75a5ffc1e87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 52,
                "offset": 3,
                "shift": 14,
                "w": 14,
                "x": 169,
                "y": 164
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9013845e-8155-4fbe-ad33-f5139319c409",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 52,
                "offset": 2,
                "shift": 13,
                "w": 13,
                "x": 185,
                "y": 164
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e56f8678-ab34-4650-8213-036baf1f25af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 52,
                "offset": 3,
                "shift": 17,
                "w": 15,
                "x": 53,
                "y": 164
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "c7f7bcbe-0516-420a-b750-b359a3d18f2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 52,
                "offset": 3,
                "shift": 18,
                "w": 15,
                "x": 36,
                "y": 164
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "871becda-7ea1-4fad-97d1-3855e2311220",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 52,
                "offset": 4,
                "shift": 16,
                "w": 15,
                "x": 19,
                "y": 164
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "5d910fa1-33aa-49d7-981d-3436df039cfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 52,
                "offset": 4,
                "shift": 23,
                "w": 22,
                "x": 481,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "74c945f0-a1f1-48a0-90b1-4224db04a0f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 52,
                "offset": 0,
                "shift": 16,
                "w": 18,
                "x": 182,
                "y": 110
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "46d525d1-7f4b-4031-8bb1-fc1d529a9a73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 52,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 335,
                "y": 110
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "111a77be-ca0d-4921-b51f-944e1fdc4952",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 52,
                "offset": 1,
                "shift": 16,
                "w": 17,
                "x": 240,
                "y": 110
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "8bfc5891-64ac-444e-b5a3-ae56e7b81fd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 52,
                "offset": 1,
                "shift": 15,
                "w": 18,
                "x": 142,
                "y": 110
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "90a495a8-201c-4c29-8fb7-5c9d4183edbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 52,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 215,
                "y": 164
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f9b46af3-4bd2-4437-9551-959498783e51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 52,
                "offset": -3,
                "shift": 15,
                "w": 18,
                "x": 82,
                "y": 110
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "1de67780-5152-4e00-a579-a4ca9890fbab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 52,
                "offset": 3,
                "shift": 19,
                "w": 19,
                "x": 414,
                "y": 56
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
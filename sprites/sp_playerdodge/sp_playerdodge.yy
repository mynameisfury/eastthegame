{
    "id": "314e7b3a-2570-48f3-aee1-a69d8a8ff89f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_playerdodge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffe9272c-d4e8-4bd7-b5e1-83e95ebb5e8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "314e7b3a-2570-48f3-aee1-a69d8a8ff89f",
            "compositeImage": {
                "id": "847a98e7-508e-4188-896d-6cf8834238b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffe9272c-d4e8-4bd7-b5e1-83e95ebb5e8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "498ec2f2-fcfb-4051-a70b-fd10c1ce75f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffe9272c-d4e8-4bd7-b5e1-83e95ebb5e8a",
                    "LayerId": "4f7529b0-db6c-482e-b1a2-41b53b154a4c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4f7529b0-db6c-482e-b1a2-41b53b154a4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "314e7b3a-2570-48f3-aee1-a69d8a8ff89f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
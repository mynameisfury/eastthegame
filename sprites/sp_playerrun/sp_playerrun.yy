{
    "id": "38ae0eaf-50ec-4034-8591-29115189b773",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_playerrun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 19,
    "bbox_right": 45,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a9a06a0-37db-4341-8981-140a6007d254",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38ae0eaf-50ec-4034-8591-29115189b773",
            "compositeImage": {
                "id": "ead01f6a-6558-403f-88de-5d7669902ae4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a9a06a0-37db-4341-8981-140a6007d254",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e53339f9-717f-48a2-8889-800bf1fbaea1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a9a06a0-37db-4341-8981-140a6007d254",
                    "LayerId": "c913c7cf-def6-49f9-b32b-2a8f2a39d464"
                }
            ]
        },
        {
            "id": "d5e16bf7-ff05-4d41-a930-bb9c8e5ebdf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38ae0eaf-50ec-4034-8591-29115189b773",
            "compositeImage": {
                "id": "557f2350-38ef-4f77-b829-eb1ebb22c2a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5e16bf7-ff05-4d41-a930-bb9c8e5ebdf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cad3ea13-7a7d-4f28-a4f4-16d2cb2c7c89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5e16bf7-ff05-4d41-a930-bb9c8e5ebdf7",
                    "LayerId": "c913c7cf-def6-49f9-b32b-2a8f2a39d464"
                }
            ]
        },
        {
            "id": "3b5e2d54-11ae-4f79-bf56-cd76e9fd0969",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38ae0eaf-50ec-4034-8591-29115189b773",
            "compositeImage": {
                "id": "336be2fa-4cc0-43dd-b310-88654a3dbe13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b5e2d54-11ae-4f79-bf56-cd76e9fd0969",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2743100-0a80-4a72-b21e-9e98d9cdea24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b5e2d54-11ae-4f79-bf56-cd76e9fd0969",
                    "LayerId": "c913c7cf-def6-49f9-b32b-2a8f2a39d464"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c913c7cf-def6-49f9-b32b-2a8f2a39d464",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38ae0eaf-50ec-4034-8591-29115189b773",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
{
    "id": "0f84c21d-0912-4980-81fa-96ce279ad6f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_items",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 15,
    "bbox_right": 55,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4be0552e-3625-4013-b5d3-715af8461eee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f84c21d-0912-4980-81fa-96ce279ad6f0",
            "compositeImage": {
                "id": "85815d73-7006-41a8-a7b0-e090fc0c59b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4be0552e-3625-4013-b5d3-715af8461eee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81cd8b13-cf76-4ad7-8747-1fa355be9dd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4be0552e-3625-4013-b5d3-715af8461eee",
                    "LayerId": "79ed68ab-5011-4cff-abcb-760b28d0855f"
                }
            ]
        },
        {
            "id": "233da048-9fee-4c33-b2d5-13b6d109c4e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f84c21d-0912-4980-81fa-96ce279ad6f0",
            "compositeImage": {
                "id": "38697565-3266-449f-8b7b-7eafe9ac1b2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "233da048-9fee-4c33-b2d5-13b6d109c4e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0097dff5-6b21-4f48-a592-984c407cfb97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "233da048-9fee-4c33-b2d5-13b6d109c4e6",
                    "LayerId": "79ed68ab-5011-4cff-abcb-760b28d0855f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "79ed68ab-5011-4cff-abcb-760b28d0855f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f84c21d-0912-4980-81fa-96ce279ad6f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
{
    "id": "0d86a2f2-8864-418c-b364-3d013ec1c224",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_assaultrifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 39,
    "bbox_right": 63,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f0b72c9-6336-4b07-8264-6a34fd407e56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d86a2f2-8864-418c-b364-3d013ec1c224",
            "compositeImage": {
                "id": "2b3072eb-3c18-4755-92ef-6a3e532cb958",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f0b72c9-6336-4b07-8264-6a34fd407e56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a927e2b2-2f2b-4188-b4b0-1f78698051c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f0b72c9-6336-4b07-8264-6a34fd407e56",
                    "LayerId": "a7c12a6f-aa48-44f8-8f85-d3df258a12b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a7c12a6f-aa48-44f8-8f85-d3df258a12b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d86a2f2-8864-418c-b364-3d013ec1c224",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 53,
    "yorig": 21
}
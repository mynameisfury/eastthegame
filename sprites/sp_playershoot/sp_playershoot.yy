{
    "id": "1db87825-d73c-45dc-a24a-12ab5325eba4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_playershoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 15,
    "bbox_right": 42,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2896ed2b-325e-47d2-9234-2f0995ddef1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1db87825-d73c-45dc-a24a-12ab5325eba4",
            "compositeImage": {
                "id": "c9ec3d46-6a46-48c9-9e01-4b3350bd4e39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2896ed2b-325e-47d2-9234-2f0995ddef1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe3837ba-1aea-44ce-8aeb-56bcd5c224bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2896ed2b-325e-47d2-9234-2f0995ddef1a",
                    "LayerId": "26458557-86b9-4739-a525-d334f5553e02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "26458557-86b9-4739-a525-d334f5553e02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1db87825-d73c-45dc-a24a-12ab5325eba4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
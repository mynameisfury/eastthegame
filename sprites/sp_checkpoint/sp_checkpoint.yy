{
    "id": "462b3aa9-0979-45b7-9e40-c387d9d87fec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_checkpoint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99ad7ce1-ef57-47df-b04e-07cf4f36596c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "462b3aa9-0979-45b7-9e40-c387d9d87fec",
            "compositeImage": {
                "id": "b8970ba2-c307-4ee8-a03d-2e3521e3ab7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99ad7ce1-ef57-47df-b04e-07cf4f36596c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0042bd0a-dc10-432c-8700-9bd51739eaad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99ad7ce1-ef57-47df-b04e-07cf4f36596c",
                    "LayerId": "8ca8cfd2-69a4-450a-87b3-b2ea2e347d3a"
                }
            ]
        },
        {
            "id": "ce3dd47f-c565-473c-8e09-2d5bd6e6ec2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "462b3aa9-0979-45b7-9e40-c387d9d87fec",
            "compositeImage": {
                "id": "37348ada-299b-4a30-ac35-44520ef24f29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce3dd47f-c565-473c-8e09-2d5bd6e6ec2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aabac0f8-4f3d-46ad-86c5-e059243dfea9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce3dd47f-c565-473c-8e09-2d5bd6e6ec2a",
                    "LayerId": "8ca8cfd2-69a4-450a-87b3-b2ea2e347d3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8ca8cfd2-69a4-450a-87b3-b2ea2e347d3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "462b3aa9-0979-45b7-9e40-c387d9d87fec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
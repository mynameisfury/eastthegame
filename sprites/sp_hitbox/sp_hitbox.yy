{
    "id": "7ce70d53-1e8a-459e-a2cb-ca0ad7efc693",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_hitbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42ab4196-41b2-489c-aeff-574a9cd66dde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ce70d53-1e8a-459e-a2cb-ca0ad7efc693",
            "compositeImage": {
                "id": "a0b821b9-d5a9-474f-a075-546dac877875",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42ab4196-41b2-489c-aeff-574a9cd66dde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f87fbd1-2405-4e91-8bdb-90a702ae6313",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42ab4196-41b2-489c-aeff-574a9cd66dde",
                    "LayerId": "312b4411-6f51-45ea-9464-9b912dd2473f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "312b4411-6f51-45ea-9464-9b912dd2473f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ce70d53-1e8a-459e-a2cb-ca0ad7efc693",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
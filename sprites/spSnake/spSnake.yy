{
    "id": "20a23bfa-3e86-4bf9-8016-0db915dac397",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spSnake",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "279861bc-ce70-48ae-b8b0-d101eb6584eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20a23bfa-3e86-4bf9-8016-0db915dac397",
            "compositeImage": {
                "id": "50422b3a-fee1-4a93-ab7c-bc85da6364b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "279861bc-ce70-48ae-b8b0-d101eb6584eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77d0de0e-0430-4c05-9852-e283910516e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "279861bc-ce70-48ae-b8b0-d101eb6584eb",
                    "LayerId": "1352c6db-3295-4a15-bfa7-9a56698b47d1"
                }
            ]
        },
        {
            "id": "a5a45e5a-65cf-4983-98ea-6845564d4368",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20a23bfa-3e86-4bf9-8016-0db915dac397",
            "compositeImage": {
                "id": "11e0fbc0-ea42-457a-8fa8-f547bd85a787",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5a45e5a-65cf-4983-98ea-6845564d4368",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86ea87cd-56b3-465b-8952-16955d2e7111",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5a45e5a-65cf-4983-98ea-6845564d4368",
                    "LayerId": "1352c6db-3295-4a15-bfa7-9a56698b47d1"
                }
            ]
        },
        {
            "id": "36db1bf0-7457-410b-ad63-ef6d7e56a410",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20a23bfa-3e86-4bf9-8016-0db915dac397",
            "compositeImage": {
                "id": "5978ad03-663c-49d5-979b-c8bc8d723990",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36db1bf0-7457-410b-ad63-ef6d7e56a410",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f966cf9-9285-46fe-8a49-7fecd142cd37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36db1bf0-7457-410b-ad63-ef6d7e56a410",
                    "LayerId": "1352c6db-3295-4a15-bfa7-9a56698b47d1"
                }
            ]
        },
        {
            "id": "cee796a3-1b15-445d-9f4e-6606114eee0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20a23bfa-3e86-4bf9-8016-0db915dac397",
            "compositeImage": {
                "id": "39c77b82-e1f2-4c1a-89c7-74624afb7316",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cee796a3-1b15-445d-9f4e-6606114eee0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4e0c559-f123-4c23-88ce-2f1d0446751f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cee796a3-1b15-445d-9f4e-6606114eee0b",
                    "LayerId": "1352c6db-3295-4a15-bfa7-9a56698b47d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1352c6db-3295-4a15-bfa7-9a56698b47d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20a23bfa-3e86-4bf9-8016-0db915dac397",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 19,
    "yorig": 5
}
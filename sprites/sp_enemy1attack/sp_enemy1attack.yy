{
    "id": "69241c3b-1ac6-42cd-a47d-b4784bef65bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_enemy1attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 5,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f6d308d-c443-4020-905b-7565a5e1d818",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69241c3b-1ac6-42cd-a47d-b4784bef65bd",
            "compositeImage": {
                "id": "8350c22d-3bdc-4277-a472-721c441679e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f6d308d-c443-4020-905b-7565a5e1d818",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16e3cd9a-e0ae-41c9-9ddb-3715838847f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f6d308d-c443-4020-905b-7565a5e1d818",
                    "LayerId": "b34b2282-2bdf-4c80-b5aa-0ef72ee7cecc"
                }
            ]
        },
        {
            "id": "ad1c0611-bfed-4e9f-8048-142c7f064292",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69241c3b-1ac6-42cd-a47d-b4784bef65bd",
            "compositeImage": {
                "id": "c4ebcba0-0567-4cec-b663-f71735a4c832",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad1c0611-bfed-4e9f-8048-142c7f064292",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00d4e92f-1273-456a-a581-013ca50cf986",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad1c0611-bfed-4e9f-8048-142c7f064292",
                    "LayerId": "b34b2282-2bdf-4c80-b5aa-0ef72ee7cecc"
                }
            ]
        },
        {
            "id": "a0a03608-0ba7-46a7-a3aa-d59c6f0846a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69241c3b-1ac6-42cd-a47d-b4784bef65bd",
            "compositeImage": {
                "id": "3b9b93ea-57ec-478e-add8-cb9a928be62e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0a03608-0ba7-46a7-a3aa-d59c6f0846a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "556b2dd8-1412-4d92-b009-d76a22518c03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0a03608-0ba7-46a7-a3aa-d59c6f0846a2",
                    "LayerId": "b34b2282-2bdf-4c80-b5aa-0ef72ee7cecc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b34b2282-2bdf-4c80-b5aa-0ef72ee7cecc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69241c3b-1ac6-42cd-a47d-b4784bef65bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
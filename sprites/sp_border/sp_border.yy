{
    "id": "162e8fb5-0d62-4313-852c-8b7d62fac997",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_border",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b6bee5c-5b26-45e4-970f-61a1ba396053",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "162e8fb5-0d62-4313-852c-8b7d62fac997",
            "compositeImage": {
                "id": "928d502d-ab3e-4af2-8271-7cee3009ee38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b6bee5c-5b26-45e4-970f-61a1ba396053",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bea6e340-d201-441d-83b3-bdfdd26dfa89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b6bee5c-5b26-45e4-970f-61a1ba396053",
                    "LayerId": "df3017d9-ea8b-4ed1-802a-d8a73bcc131c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "df3017d9-ea8b-4ed1-802a-d8a73bcc131c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "162e8fb5-0d62-4313-852c-8b7d62fac997",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
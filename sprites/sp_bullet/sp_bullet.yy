{
    "id": "4bbf4a99-02ea-44b7-b42f-16911e92c988",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 29,
    "bbox_right": 34,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bac60f6-18bd-4347-b8d8-3d71208578ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bbf4a99-02ea-44b7-b42f-16911e92c988",
            "compositeImage": {
                "id": "6525d9f1-3cbe-48df-a101-322605e43b3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bac60f6-18bd-4347-b8d8-3d71208578ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd0d1c71-16a3-46b8-acb0-8a62958e2a97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bac60f6-18bd-4347-b8d8-3d71208578ed",
                    "LayerId": "207b5192-c084-49e3-a975-2b050ad4e97b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "207b5192-c084-49e3-a975-2b050ad4e97b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4bbf4a99-02ea-44b7-b42f-16911e92c988",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 40
}
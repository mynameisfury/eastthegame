{
    "id": "4513a6cd-1a0d-44bd-881a-7c02329c0f73",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_hud",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 715,
    "bbox_left": 196,
    "bbox_right": 807,
    "bbox_top": 653,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "196597a7-8e8b-4b53-9d5a-d5a7138bcf0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4513a6cd-1a0d-44bd-881a-7c02329c0f73",
            "compositeImage": {
                "id": "ac85c7bc-0af4-4345-87ab-5623c4199688",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "196597a7-8e8b-4b53-9d5a-d5a7138bcf0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21f58d46-30bc-4d5c-83fa-6b7bffd24df4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "196597a7-8e8b-4b53-9d5a-d5a7138bcf0f",
                    "LayerId": "e8cf27f1-cd18-4ade-9e14-4f7d93b32284"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "e8cf27f1-cd18-4ade-9e14-4f7d93b32284",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4513a6cd-1a0d-44bd-881a-7c02329c0f73",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}
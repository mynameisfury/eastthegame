{
    "id": "2387ec33-59fb-4414-a6c1-a0fdccd52db4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_platform1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d5434ca-4cb3-46db-b3e8-55167a864a1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2387ec33-59fb-4414-a6c1-a0fdccd52db4",
            "compositeImage": {
                "id": "c61a2722-a93c-4a49-8a72-8c9d48a1ef44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d5434ca-4cb3-46db-b3e8-55167a864a1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef11b648-cea5-4d84-8fa0-13de705605a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d5434ca-4cb3-46db-b3e8-55167a864a1f",
                    "LayerId": "b8915058-0357-48bd-9ffb-fe04a305bf7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b8915058-0357-48bd-9ffb-fe04a305bf7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2387ec33-59fb-4414-a6c1-a0fdccd52db4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
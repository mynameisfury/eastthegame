{
    "id": "94f6082c-2d72-4619-8309-ee5030912a30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_trigger",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eeda90a4-d2f6-449c-a01d-b81f10a0848c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94f6082c-2d72-4619-8309-ee5030912a30",
            "compositeImage": {
                "id": "01dc7c03-2cab-4f28-b136-7227a78d7675",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eeda90a4-d2f6-449c-a01d-b81f10a0848c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3833309b-9424-49a5-a238-088e44dbb6bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eeda90a4-d2f6-449c-a01d-b81f10a0848c",
                    "LayerId": "8c08b894-775b-4235-bd3b-4064976e769f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8c08b894-775b-4235-bd3b-4064976e769f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94f6082c-2d72-4619-8309-ee5030912a30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
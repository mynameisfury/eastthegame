{
    "id": "91e4093f-56a3-44fa-9ad0-835fec8eb250",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_enemy1run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 5,
    "bbox_right": 58,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3548119c-33ca-4853-9d51-0b2c439426e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91e4093f-56a3-44fa-9ad0-835fec8eb250",
            "compositeImage": {
                "id": "b87665f6-5762-4c19-9e43-f36ebc0ddcc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3548119c-33ca-4853-9d51-0b2c439426e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e3f31b0-a08f-42c4-b94f-a9e4ddff9879",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3548119c-33ca-4853-9d51-0b2c439426e9",
                    "LayerId": "a165f335-7570-4bad-bc41-0f4c0babc7ae"
                }
            ]
        },
        {
            "id": "dfc25b90-bfed-4cf4-821e-60f2dc8f6fcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91e4093f-56a3-44fa-9ad0-835fec8eb250",
            "compositeImage": {
                "id": "27f11e5a-25c7-42ab-b754-979096178c44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfc25b90-bfed-4cf4-821e-60f2dc8f6fcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71a4ab4b-0e22-4d09-81f6-bf63acf2761a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfc25b90-bfed-4cf4-821e-60f2dc8f6fcb",
                    "LayerId": "a165f335-7570-4bad-bc41-0f4c0babc7ae"
                }
            ]
        },
        {
            "id": "aeceb8a8-070e-4961-9355-318967c87efd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91e4093f-56a3-44fa-9ad0-835fec8eb250",
            "compositeImage": {
                "id": "bcdbcbd0-aad3-4554-b510-13615b8bff45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aeceb8a8-070e-4961-9355-318967c87efd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1882f223-48d2-4bb0-8d98-3f850ca3c849",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aeceb8a8-070e-4961-9355-318967c87efd",
                    "LayerId": "a165f335-7570-4bad-bc41-0f4c0babc7ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a165f335-7570-4bad-bc41-0f4c0babc7ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91e4093f-56a3-44fa-9ad0-835fec8eb250",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
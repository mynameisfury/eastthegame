{
    "id": "7e1077c4-0ff3-4272-983f-655e180425b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_deathmachine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 15,
    "bbox_right": 55,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c1de346-1cb4-4c32-8b91-b08610ec3740",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e1077c4-0ff3-4272-983f-655e180425b4",
            "compositeImage": {
                "id": "213fbd39-bc0e-4227-95cc-618af594900b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c1de346-1cb4-4c32-8b91-b08610ec3740",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3c4c198-9007-4683-b865-b1e61368b3a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c1de346-1cb4-4c32-8b91-b08610ec3740",
                    "LayerId": "bab82e01-1260-4192-9a02-9a90995a7c85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bab82e01-1260-4192-9a02-9a90995a7c85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e1077c4-0ff3-4272-983f-655e180425b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
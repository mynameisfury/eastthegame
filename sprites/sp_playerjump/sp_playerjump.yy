{
    "id": "6247332c-a94e-4a63-b3c3-09f7bd8bff4c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_playerjump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 18,
    "bbox_right": 48,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed005edd-0149-4fe8-ad4d-6b4b4d12d70d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6247332c-a94e-4a63-b3c3-09f7bd8bff4c",
            "compositeImage": {
                "id": "bb98ddf0-3c3f-4848-a53a-96ac93a6d78f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed005edd-0149-4fe8-ad4d-6b4b4d12d70d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1c2f592-9d24-466d-a551-0616a72f377c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed005edd-0149-4fe8-ad4d-6b4b4d12d70d",
                    "LayerId": "4171f0a6-93d0-4980-a53c-13420cf8630a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4171f0a6-93d0-4980-a53c-13420cf8630a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6247332c-a94e-4a63-b3c3-09f7bd8bff4c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 38,
    "yorig": 30
}
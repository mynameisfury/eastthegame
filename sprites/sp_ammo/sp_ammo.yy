{
    "id": "4ca69066-adf0-4750-99c4-1f415ea913d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_ammo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "287165f0-bca7-4fcf-871e-a09c14370ee9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ca69066-adf0-4750-99c4-1f415ea913d9",
            "compositeImage": {
                "id": "d98891e6-882d-481b-9522-01a089003889",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "287165f0-bca7-4fcf-871e-a09c14370ee9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48e338fd-f4f2-4bde-8ead-24e27627cdb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "287165f0-bca7-4fcf-871e-a09c14370ee9",
                    "LayerId": "1b3eef2a-bef2-4f8f-b105-b3c5a7161572"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "1b3eef2a-bef2-4f8f-b105-b3c5a7161572",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ca69066-adf0-4750-99c4-1f415ea913d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}
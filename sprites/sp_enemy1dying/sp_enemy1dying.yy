{
    "id": "b009bdf4-f7c6-4888-b927-d924df6bd06c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_enemy1dying",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 5,
    "bbox_right": 48,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f45800d-83f2-4058-b482-2141de549553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b009bdf4-f7c6-4888-b927-d924df6bd06c",
            "compositeImage": {
                "id": "9f8aac3e-f8ff-420e-a14b-0f7cc95063d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f45800d-83f2-4058-b482-2141de549553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba875d77-f8bb-477b-bdc6-2423b0d21b98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f45800d-83f2-4058-b482-2141de549553",
                    "LayerId": "58f7df36-be34-42a3-8e48-15be600d6376"
                }
            ]
        },
        {
            "id": "d6db2895-5efb-4910-a813-1199ad97827b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b009bdf4-f7c6-4888-b927-d924df6bd06c",
            "compositeImage": {
                "id": "de4b0634-fa2e-475f-94ba-ed342ac51474",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6db2895-5efb-4910-a813-1199ad97827b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b089026-3181-4afb-8ac0-a278a21997de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6db2895-5efb-4910-a813-1199ad97827b",
                    "LayerId": "58f7df36-be34-42a3-8e48-15be600d6376"
                }
            ]
        },
        {
            "id": "5fd5a4f4-d0e2-4312-b2e2-6ebeea4d1ed0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b009bdf4-f7c6-4888-b927-d924df6bd06c",
            "compositeImage": {
                "id": "1c2cb04b-514b-433f-83d5-5791d299e361",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fd5a4f4-d0e2-4312-b2e2-6ebeea4d1ed0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d7471cd-a22b-45ad-b882-6b68745a72c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fd5a4f4-d0e2-4312-b2e2-6ebeea4d1ed0",
                    "LayerId": "58f7df36-be34-42a3-8e48-15be600d6376"
                }
            ]
        },
        {
            "id": "9efe5a5c-94c2-4de5-ab90-4e3878b373c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b009bdf4-f7c6-4888-b927-d924df6bd06c",
            "compositeImage": {
                "id": "f46b0f55-8fa4-4a63-bc8e-bd9de6600bad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9efe5a5c-94c2-4de5-ab90-4e3878b373c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f1d9ca7-5187-4664-ada8-7753f52e5b73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9efe5a5c-94c2-4de5-ab90-4e3878b373c4",
                    "LayerId": "58f7df36-be34-42a3-8e48-15be600d6376"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "58f7df36-be34-42a3-8e48-15be600d6376",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b009bdf4-f7c6-4888-b927-d924df6bd06c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
{
    "id": "4ae57654-beb5-4894-af69-9ca4091e4d42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_deathscreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 744,
    "bbox_left": 115,
    "bbox_right": 928,
    "bbox_top": 211,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33e12fd2-48ac-4672-8057-197b62e6d186",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ae57654-beb5-4894-af69-9ca4091e4d42",
            "compositeImage": {
                "id": "a6103f61-3ffa-4090-b851-4ebfc72c0b44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33e12fd2-48ac-4672-8057-197b62e6d186",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50bc8ef9-e5f3-4f15-81e5-91a4395e9400",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33e12fd2-48ac-4672-8057-197b62e6d186",
                    "LayerId": "b1bf9fb7-e750-48ab-b922-67f1f3a24dbd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "b1bf9fb7-e750-48ab-b922-67f1f3a24dbd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ae57654-beb5-4894-af69-9ca4091e4d42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}
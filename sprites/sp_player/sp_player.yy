{
    "id": "a2643eba-ab2b-441a-b03a-0df9536afc38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 20,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca144022-e622-4c50-ab6c-9d7e683c8c35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2643eba-ab2b-441a-b03a-0df9536afc38",
            "compositeImage": {
                "id": "d86ddade-5d75-4a3b-af92-dcfe954c1f4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca144022-e622-4c50-ab6c-9d7e683c8c35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbee62ed-4e3d-4c87-853b-0fbbd11b48b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca144022-e622-4c50-ab6c-9d7e683c8c35",
                    "LayerId": "7013fe0e-a20f-404d-bf2a-5bccb74892c0"
                }
            ]
        },
        {
            "id": "db6c87d1-7203-4475-92e2-32888821a020",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2643eba-ab2b-441a-b03a-0df9536afc38",
            "compositeImage": {
                "id": "15ef3eda-d4b7-4895-b56a-70ea5028bd6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db6c87d1-7203-4475-92e2-32888821a020",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2496a770-e744-4598-84a7-757d09c183bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db6c87d1-7203-4475-92e2-32888821a020",
                    "LayerId": "7013fe0e-a20f-404d-bf2a-5bccb74892c0"
                }
            ]
        },
        {
            "id": "8ca9b66d-2bdc-4737-bdfb-6a76ba9d4405",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2643eba-ab2b-441a-b03a-0df9536afc38",
            "compositeImage": {
                "id": "306d0568-3182-4f86-9533-99d7f5732db8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ca9b66d-2bdc-4737-bdfb-6a76ba9d4405",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb49a8b0-7e1d-44fa-b719-1f282ddf2f34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ca9b66d-2bdc-4737-bdfb-6a76ba9d4405",
                    "LayerId": "7013fe0e-a20f-404d-bf2a-5bccb74892c0"
                }
            ]
        },
        {
            "id": "5e2e73fb-3b39-454f-85f7-90ce55263388",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2643eba-ab2b-441a-b03a-0df9536afc38",
            "compositeImage": {
                "id": "1af6a58d-24c8-4c4f-add1-241ea39041d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e2e73fb-3b39-454f-85f7-90ce55263388",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f70c70c3-161d-4312-9631-53a9a81439d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e2e73fb-3b39-454f-85f7-90ce55263388",
                    "LayerId": "7013fe0e-a20f-404d-bf2a-5bccb74892c0"
                }
            ]
        },
        {
            "id": "07093768-491a-48a3-821f-805ecc2538f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2643eba-ab2b-441a-b03a-0df9536afc38",
            "compositeImage": {
                "id": "070c629e-0bd7-4f0d-80d5-03988b70024f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07093768-491a-48a3-821f-805ecc2538f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "149ec7df-6564-4e44-b55a-8efda4a10241",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07093768-491a-48a3-821f-805ecc2538f6",
                    "LayerId": "7013fe0e-a20f-404d-bf2a-5bccb74892c0"
                }
            ]
        },
        {
            "id": "51565951-e619-4deb-8e72-606560194433",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2643eba-ab2b-441a-b03a-0df9536afc38",
            "compositeImage": {
                "id": "b37e50b2-4ad5-468d-8093-9fa75781e5da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51565951-e619-4deb-8e72-606560194433",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8949fc7-0413-4337-92e4-1f4aac61ec7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51565951-e619-4deb-8e72-606560194433",
                    "LayerId": "7013fe0e-a20f-404d-bf2a-5bccb74892c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7013fe0e-a20f-404d-bf2a-5bccb74892c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2643eba-ab2b-441a-b03a-0df9536afc38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
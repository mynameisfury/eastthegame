//player input
if (hascontrol)
{
key_left = keyboard_check(vk_left);
key_right = keyboard_check(vk_right);
key_jump = keyboard_check_pressed(vk_up);
key_shoot1 = vk_space;
key_defense = keyboard_check(vk_lshift);
}
else
{
	key_jump = 0;
	key_right = 0;
	key_left = 0;
}


//hp
if hp <= 0 
{
	instance_destroy();
	
}
//calculate movement
var move = key_right - key_left;

hsp = move * walksp;

vsp = vsp + grv;

if (place_meeting(x,y+1,objWall)) and (key_jump)
{
	vsp = -19;
}
//defense
if defensetime = 0
{
	defensecooldown = defensecooldown + 1;
}
if defensecooldown = 300
{
	defensetime = 15;
}

if (key_defense) and defensetime >0
    {
        hsp = 20 * image_xscale;
		defensetime = defensetime - 1;
		defensecooldown = 0;
    }


//horizontal collision
if (place_meeting(x + hsp,y,objWall))
{
	while (!place_meeting(x+sign(hsp),y,objWall))
	{
		x = x + sign(hsp);
	}
	hsp = 0;
}
if (place_meeting(x + hsp,y,obj_stickyplatform))
{
	while (!place_meeting(x+sign(hsp),y,obj_stickyplatform))
	{
		x = x + sign(hsp);
	}
	hsp = 0;
}
x = x +hsp

//vertical collision
if (place_meeting(x ,y+ vsp,objWall))
{
	while (!place_meeting(x,y+sign(vsp),objWall))
	{
		y = y + sign(vsp);
	}
	vsp = 0;
}
if (place_meeting(x ,y+ vsp,obj_stickyplatform))
{
	while (!place_meeting(x,y+sign(vsp),obj_stickyplatform))
	{
		y = y + sign(vsp);
	}
	vsp = 0;
}
y = y + vsp;

//animation
if (!place_meeting(x,y+1, objWall))
{
	sprite_index = sp_playerjump;
	image_speed = 0;
	if (sign(vsp) > 0) image_index = 1; else image_index = 0;
}
else
{
	image_speed = 1;
	if hsp != 0
	{	
		sprite_index= sp_playerrun;
	}	
	else 
	{
		sprite_index = sp_player;
	}
}

//defense animation

if (hsp = 20 *image_xscale)
{
	sprite_index = sp_playerdodge;
}

if (hsp !=0) image_xscale = sign (hsp);


//guns
if (keyboard_check (key_shoot1))
{
	sprite_index = sp_playershoot;
}

/*
firingdelay = firingdelay - 1
if (keyboard_check(key_shoot1)) and (firingdelay <0)
{
	sprite_index = sp_playershoot
	firingdelay = 8
	with (instance_create_layer(x,y,"bullets", objBullet))
	{
		speed = 20		
	}
}

*/
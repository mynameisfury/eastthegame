{
    "id": "b73fd4ae-4b7a-463d-9b3c-64a7dcefacd8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPlayer",
    "eventList": [
        {
            "id": "0d87de2d-69ab-4557-8e9e-3b49a6a2debf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b73fd4ae-4b7a-463d-9b3c-64a7dcefacd8"
        },
        {
            "id": "6296f63c-150b-4e78-992e-6b850f454783",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b73fd4ae-4b7a-463d-9b3c-64a7dcefacd8"
        },
        {
            "id": "e0a8a015-904b-4023-b1c9-5ef729dfe2e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "81be2aa6-8862-44f6-9081-9270a355a559",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b73fd4ae-4b7a-463d-9b3c-64a7dcefacd8"
        },
        {
            "id": "0c1a7973-a577-4638-b72c-c03cb6ec5b13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "62be7d83-8e5b-43b3-839a-8335fadd6e2e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b73fd4ae-4b7a-463d-9b3c-64a7dcefacd8"
        },
        {
            "id": "7e8848c9-b433-4fc9-80bb-199db217c0b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "b73fd4ae-4b7a-463d-9b3c-64a7dcefacd8"
        },
        {
            "id": "3996b7ca-842d-46e2-8d3f-68df5a1c16ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c4a65696-dafd-44ce-808c-6fd16d3e71c4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b73fd4ae-4b7a-463d-9b3c-64a7dcefacd8"
        }
    ],
    "maskSpriteId": "a2643eba-ab2b-441a-b03a-0df9536afc38",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a2643eba-ab2b-441a-b03a-0df9536afc38",
    "visible": true
}
{
    "id": "37b889f1-406f-42f2-b24a-8c66f00948f6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objEnemy2",
    "eventList": [
        {
            "id": "a37efd8c-0409-422c-9230-294dd473737d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "37b889f1-406f-42f2-b24a-8c66f00948f6"
        },
        {
            "id": "945a7d5b-f96c-4222-a35e-1278d9ea9710",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "37b889f1-406f-42f2-b24a-8c66f00948f6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "462b3aa9-0979-45b7-9e40-c387d9d87fec",
    "visible": true
}
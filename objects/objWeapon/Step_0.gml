other.x = objPlayer.x;
other.y = objPlayer.y - 10;

///face bullets correctly
if objPlayer.image_xscale = -1.00 
	{
		direction = 180;
	}
	else
	{
		direction = 0; 
	}
	//bullet stuff
if (direction > 90) and (direction < 270) 
	{
		image_yscale = -1 
	}
else 
	{
		image_yscale = 1
	}
image_angle = direction
if (automatic == true) 
{
	key_shoot1 = keyboard_check(vk_space); 
}
else 
{
	key_shoot1 = keyboard_check_pressed(vk_space);
}
if (key_shoot1)
{
	if (currentCooldown = 0)
	{		
		currentCooldown = firingdelay;
		currentDelay = startup;
	}
}

if (currentDelay == 0) 
{
	if (ammo[weapon] !=0)
	{
		with (instance_create_layer(x+lengthdir_x(length,direction), y+lengthdir_y(length,direction), "bullets", objBullet))
		{	
			direction = other.direction;
			speed = other.bulletspeed;
		}
		ammo[weapon] -=1;
	}
}

currentDelay = max(-1,currentDelay-1);
if (currentDelay == -1)
{
	currentCooldown = max(0,currentCooldown-1);
}
currentRecoil = max(0,floor(currentRecoil*0.8));

depth = objPlayer.depth-1;

if (keyboard_check_pressed(ord("2")))
{
	ChangeWeapon(currentWeapon1);
}
if (keyboard_check_pressed(ord("3")))
{
	ChangeWeapon(currentWeapon2);
}



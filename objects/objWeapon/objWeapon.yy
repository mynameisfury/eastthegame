{
    "id": "5df232ad-d563-4374-a56e-c715426b49ad",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objWeapon",
    "eventList": [
        {
            "id": "946b9d5e-fc89-4cc8-8fc0-494ab267358b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5df232ad-d563-4374-a56e-c715426b49ad"
        },
        {
            "id": "2829fd3e-7c25-4db0-9baf-a5ca82cc8bcb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5df232ad-d563-4374-a56e-c715426b49ad"
        },
        {
            "id": "996f0da2-eb86-404c-bdf9-8227a0b3308b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5df232ad-d563-4374-a56e-c715426b49ad"
        },
        {
            "id": "10df78f7-93dd-4a62-97e0-c5bcf012f094",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "5df232ad-d563-4374-a56e-c715426b49ad"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
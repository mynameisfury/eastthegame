enum allweapons
{
	pistol = 0,
	autorifle=1,
	dualknives=2
	
}

//pistol
	weapons[0] = ds_map_create();
	ds_map_add(weapons[0], "sprite", -1);
	ds_map_add(weapons[0], "damage", 0);
	ds_map_add(weapons[0], "length", 32);
	ds_map_add(weapons[0], "firingdelay", 30);
	ds_map_add(weapons[0], "bulletspeed",8);
	ds_map_add(weapons[0], "startup", 0);
	ds_map_add(weapons[0], "radius", 0);
	ds_map_add(weapons[0], "bleed", 0);
	ds_map_add(weapons[0], "recoil", 0);
	ds_map_add(weapons[0], "automatic", 0);
//autorifle
	weapons[1] = ds_map_create();
	ds_map_add(weapons[1], "sprite", sp_assaultrifle);
	ds_map_add(weapons[1], "damage", 0);
	ds_map_add(weapons[1], "length", 32);
	ds_map_add(weapons[1], "firingdelay", 10);
	ds_map_add(weapons[1], "bulletspeed",15);
	ds_map_add(weapons[1], "startup", 0);
	ds_map_add(weapons[1], "radius", 0);
	ds_map_add(weapons[1], "bleed", 0);
	ds_map_add(weapons[1], "recoil", 0);
	ds_map_add(weapons[1], "automatic", true);
//death machine 
	weapons[2] = ds_map_create();
	ds_map_add(weapons[2], "sprite", sp_deathmachine);
	ds_map_add(weapons[2], "damage", 0);
	ds_map_add(weapons[2], "length", 0);
	ds_map_add(weapons[2], "firingdelay", 0);
	ds_map_add(weapons[2], "bulletspeed",30);
	ds_map_add(weapons[2], "startup", 0);
	ds_map_add(weapons[2], "radius", 0);
	ds_map_add(weapons[2], "bleed", 0);
	ds_map_add(weapons[2], "recoil", 0);
	ds_map_add(weapons[2], "automatic", true);

	weapon = 0;
	ammo [array_length_1d(weapons)-1] = 0;
	ammo[0] = 20;

	ChangeWeapon(0);

	currentCooldown = 0;
	currentDelay = -1;
	currentRecoil = 0;

currentWeapon1 = weapon;
currentWeapon2 = weapon;

{
    "id": "26ded7b5-3e10-47c9-8cbe-a05070930ef9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBullet",
    "eventList": [
        {
            "id": "5493d514-dd8b-4245-bcc3-5797c6b64e83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "26ded7b5-3e10-47c9-8cbe-a05070930ef9"
        },
        {
            "id": "d0cd4c1d-0c66-4110-bbbf-d456c5e2be09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "26ded7b5-3e10-47c9-8cbe-a05070930ef9"
        },
        {
            "id": "1856d1c3-e2a6-4618-90d4-43883d1b355e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e460f2cc-bea0-45ed-9442-5587cedd4f4b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "26ded7b5-3e10-47c9-8cbe-a05070930ef9"
        },
        {
            "id": "e79b977d-d591-4288-a4d0-e0ee34e30b29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "26ded7b5-3e10-47c9-8cbe-a05070930ef9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4bbf4a99-02ea-44b7-b42f-16911e92c988",
    "visible": true
}
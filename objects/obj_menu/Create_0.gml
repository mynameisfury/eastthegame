#macro savefile "save.sav"

gui_height = display_get_gui_height();
gui_width = display_get_gui_width();
gui_margin = 32;
menu_x = gui_width - 200;
menu_y = gui_height - gui_margin;
menu_x_target = gui_width - gui_margin;
menu_font = f_menu;
menu_itemheight = font_get_size(f_menu);
menu_committed = -1;
menu_control = true;

menu[4] = "New Game";
menu[3] = "Continue";
menu[2] = "Challenge Mode";
menu[1] = "Options";
menu[0] = "Exit";

menu_items = array_length_1d(menu);
menu_cursor = 4;

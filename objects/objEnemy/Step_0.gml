//movement
vsp = vsp + grv;

//horizontal collision
if (place_meeting(x + hsp,y,objWall))
{
	while (!place_meeting(x+sign(hsp),y,objWall))
	{
		x = x + sign(hsp);
	}
	hsp = -hsp;
}

x += hsp;

//vertical collision
if (place_meeting(x ,y + vsp,objWall))
{
	while (!place_meeting(x,y+sign(vsp),objWall))
	{
		y = y + sign(vsp);
	}
	vsp = 0;
}

y += vsp

//animation
if (hsp != 0) 
{
	image_xscale = sign (hsp);
}

//bullets
if (hp = 0) 
{
	instance_destroy(other);
}
if(enemyRecoil != -1)
{
    enemyRecoil -= 1;
   speed = recoilSpeed;
   enemyStop = 1;
}

if(enemyRecoil = -1 and enemyStop = 1)
{
    speed = 0;
    enemyStop =- 1;
}
//ai_____________________________

//state conditions
if (instance_exists(objPlayer))
{
	if (distance_to_object(objPlayer) < 300)
		{
			currentstate = enemy_state.chasing;
		}
	if (distance_to_object(objPlayer) < 5)
	{
		currentstate = enemy_state.attacking;
	}
}

//state functions
if (currentstate = enemy_state.chasing)
{
	sprite_index = sp_enemy1run;
	//mp_potential_step(objPlayer.x, y, 3,false)
	if (objPlayer.x > objEnemy.x)
	{
		hsp = 1;
		image_xscale = 1;
	}
	else if (objPlayer.x < objEnemy.x)
	{
		hsp = -1;
		image_xscale = -1;
	}
	
}

if (currentstate = enemy_state.attacking)
{
	sprite_index = sp_enemy1attack;
	if (image_index = 2)
	{
		instance_create_layer(x,y,"room",obj_hitbox);
	}	
}

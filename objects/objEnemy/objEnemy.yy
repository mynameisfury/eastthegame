{
    "id": "e460f2cc-bea0-45ed-9442-5587cedd4f4b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objEnemy",
    "eventList": [
        {
            "id": "f57ca58a-643e-4c72-b47a-3aeba38cdf0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e460f2cc-bea0-45ed-9442-5587cedd4f4b"
        },
        {
            "id": "c5ba657b-ddf2-408f-b55c-750efd0aa89f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e460f2cc-bea0-45ed-9442-5587cedd4f4b"
        },
        {
            "id": "27c825e9-adba-4628-af73-45c8ead06324",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e460f2cc-bea0-45ed-9442-5587cedd4f4b"
        },
        {
            "id": "15876382-4443-42b4-9289-8d0803824a17",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "e460f2cc-bea0-45ed-9442-5587cedd4f4b"
        }
    ],
    "maskSpriteId": "68ca2d38-98f8-43cf-bf58-26176a39f183",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "68ca2d38-98f8-43cf-bf58-26176a39f183",
    "visible": true
}
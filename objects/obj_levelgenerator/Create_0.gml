//size room
n = 50;
block_size = sprite_get_height(sp_wall);
room_set_width(room2, n * block_size);
room_set_height(room2, n* block_size);

//determine wall placement
var prob = 0.15;
layout[n - 1, n - 1] = 0;
for (var i = 0; i <n; i++)
	{
		for (var j = 0; j <n; j++)
		{
			layout[i,j] = noone;
				if (random (1.0) <= prob)
				{
					layout[i, j] = objWall;
				}
		}
	}
//go to proper rrom
room_goto(room1)

{
    "id": "a8a30ba2-1f41-4d16-b683-75c6c26c450f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_levelgenerator",
    "eventList": [
        {
            "id": "df8fbfb7-fa8d-42c7-9162-c0491069409d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a8a30ba2-1f41-4d16-b683-75c6c26c450f"
        },
        {
            "id": "18e817b1-473d-42d9-b0fb-da7b3b78d144",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a8a30ba2-1f41-4d16-b683-75c6c26c450f"
        },
        {
            "id": "9eaef6dc-fed9-406b-903c-3d7a822785ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "a8a30ba2-1f41-4d16-b683-75c6c26c450f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
{
    "id": "81be2aa6-8862-44f6-9081-9270a355a559",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objLevelEnd",
    "eventList": [
        {
            "id": "e2be0b31-98a6-4544-a9d6-8461ff40d461",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b73fd4ae-4b7a-463d-9b3c-64a7dcefacd8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "81be2aa6-8862-44f6-9081-9270a355a559"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "94f6082c-2d72-4619-8309-ee5030912a30",
    "visible": false
}
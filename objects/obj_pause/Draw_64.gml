if pause
{
	draw_set_color(c_black);
	draw_rectangle(0,0,1024,768,0);
	draw_set_halign(fa_center);
	draw_set_font(f_menu);
	draw_set_color(c_white);
	draw_text(110,110,"PAUSED");
	draw_set_color(c_black);	
}

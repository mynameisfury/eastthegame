{
    "id": "6c93a131-bb19-4d8b-9ce1-b7a62931e20f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pause",
    "eventList": [
        {
            "id": "31e758c7-6b84-4576-9338-be2220b4462d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6c93a131-bb19-4d8b-9ce1-b7a62931e20f"
        },
        {
            "id": "bec00b00-445b-42a2-af31-50890c9401c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "6c93a131-bb19-4d8b-9ce1-b7a62931e20f"
        },
        {
            "id": "d0ad2ecb-679c-4a49-84be-95192ca15783",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6c93a131-bb19-4d8b-9ce1-b7a62931e20f"
        },
        {
            "id": "18546906-6a79-4648-8cd2-3249d7b8e656",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "6c93a131-bb19-4d8b-9ce1-b7a62931e20f"
        },
        {
            "id": "0cfa400a-6af1-4fd6-95d4-437bf0a05e9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6c93a131-bb19-4d8b-9ce1-b7a62931e20f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
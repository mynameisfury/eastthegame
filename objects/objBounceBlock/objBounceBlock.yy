{
    "id": "dd09282f-c2ba-4e47-827a-ff2b54621022",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBounceBlock",
    "eventList": [
        {
            "id": "62d74b94-52be-4bfa-bd18-693a01acba10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b73fd4ae-4b7a-463d-9b3c-64a7dcefacd8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "dd09282f-c2ba-4e47-827a-ff2b54621022"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7ce70d53-1e8a-459e-a2cb-ca0ad7efc693",
    "visible": true
}
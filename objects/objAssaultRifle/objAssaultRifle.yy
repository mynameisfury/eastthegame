{
    "id": "dd3ecdf6-d582-487d-9f99-d27d85dd9ae3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objAssaultRifle",
    "eventList": [
        {
            "id": "fb121a0a-8203-43d9-9960-e6ac701305bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b73fd4ae-4b7a-463d-9b3c-64a7dcefacd8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "dd3ecdf6-d582-487d-9f99-d27d85dd9ae3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "601f9056-e638-4889-99ea-eb36b69f1863",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "73b6ad38-b534-4d3a-872d-1376cc9b58d9",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "1",
            "varName": "weapon",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "0d86a2f2-8864-418c-b364-3d013ec1c224",
    "visible": true
}
{
    "id": "cad26051-94db-4b3f-8f8a-4ab9309076d1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objEnemySnake",
    "eventList": [
        {
            "id": "2c83196d-6f33-4cd5-ba06-05fc670c6617",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cad26051-94db-4b3f-8f8a-4ab9309076d1"
        },
        {
            "id": "9eadc7bc-e974-4d28-a5ee-f5ddd698424a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cad26051-94db-4b3f-8f8a-4ab9309076d1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0b7c0b2b-de92-40a5-b701-bb01fb013e78",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "20a23bfa-3e86-4bf9-8016-0db915dac397",
    "visible": true
}
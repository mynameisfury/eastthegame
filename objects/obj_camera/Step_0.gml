/// @description update camera

// update destination
if (instance_exists(follow))
{
	xTo = follow.x;
	yTo = follow.y;
}

//update obj position

x+= (xTo - x) / 5;
y+= (yTo - y) / 5;
///camera clamp
x = clamp(x, view_w_half,room_width - view_w_half);
///update camera view
camera_set_view_pos(cam,x-view_w_half,y-view_h_half);

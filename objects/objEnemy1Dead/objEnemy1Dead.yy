{
    "id": "dc272786-6f78-445e-99c9-078b0f1f9486",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objEnemy1Dead",
    "eventList": [
        {
            "id": "c1362a33-d461-40e8-818d-666ad260a328",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dc272786-6f78-445e-99c9-078b0f1f9486"
        },
        {
            "id": "a92784a9-b3c1-4b3a-9597-ab5fc525d9b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dc272786-6f78-445e-99c9-078b0f1f9486"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b009bdf4-f7c6-4888-b927-d924df6bd06c",
    "visible": true
}
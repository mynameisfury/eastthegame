{
    "id": "bdc75e90-3859-42e8-b504-83e72a46de43",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objDeathMachine",
    "eventList": [
        {
            "id": "a78cdcb0-dd46-4b83-9b8b-aa22506a0b0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b73fd4ae-4b7a-463d-9b3c-64a7dcefacd8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bdc75e90-3859-42e8-b504-83e72a46de43"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "601f9056-e638-4889-99ea-eb36b69f1863",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7e1077c4-0ff3-4272-983f-655e180425b4",
    "visible": true
}
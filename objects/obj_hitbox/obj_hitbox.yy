{
    "id": "c4a65696-dafd-44ce-808c-6fd16d3e71c4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hitbox",
    "eventList": [
        {
            "id": "dd7854a5-f442-4c74-8f11-e7e0e77a176f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "c4a65696-dafd-44ce-808c-6fd16d3e71c4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7ce70d53-1e8a-459e-a2cb-ca0ad7efc693",
    "visible": true
}
{
    "id": "bbe012ed-b4e9-4520-918f-3b88f3f4c00d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objLevelEnd1",
    "eventList": [
        {
            "id": "dcfb89fa-5bd4-43b6-90fa-e4b3f9709445",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bbe012ed-b4e9-4520-918f-3b88f3f4c00d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "462b3aa9-0979-45b7-9e40-c387d9d87fec",
    "visible": false
}
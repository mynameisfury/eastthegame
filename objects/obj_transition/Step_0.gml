	///progress transition

		if (mode != trans_mode.off)
		{
			if (mode == trans_mode.intro)
			{
				percent = max(0,percent- max((percent/10),0.005));
			}
			else
			{
				percent = 1;
			}

			if (percent == 1) || (percent == 0)
			{
				switch (mode)
				{
					case trans_mode.intro:
					{
						hascontrol = false;
						mode = trans_mode.off;
						break;
					}
					case trans_mode.next:
					{
						hascontrol= false;
						mode = trans_mode.intro;
						room_goto_next();
						break;
					}
					case trans_mode.goto:
					{
						hascontrol= false;
						mode = trans_mode.intro;						
						room_goto(target);
						break;
					}
					case trans_mode.restart:
					{
						mode = trans_mode.intro;
						game_restart();;
						break;
					}
				}
			}
		}

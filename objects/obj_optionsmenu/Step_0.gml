// control menu

if (menu_control)
	{
	
		if keyboard_check_pressed(vk_up)
		{
			menu_cursor ++;	
			if (menu_cursor >= menu_items) menu_cursor = 0;
		}
		
		if keyboard_check_pressed(vk_down)
		{
			menu_cursor = menu_cursor - 1;
			if (menu_cursor <0) menu_cursor = menu_items -1;
		}
		
		if keyboard_check_pressed(vk_space)
		{
			menu_committed = menu_cursor;
			menu_control = false;
		}
	}

if menu_committed != -1
{
	switch (menu_committed)
	{
		case 3: default: SlideTransiton(trans_mode.goto, room1); break;
		case 2: 
		{
			if (!file_exists(savefile))
			{
				SlideTransiton(trans_mode.goto, room1);
			}
			
			else
			{
				var file = file_text_open_read(savefile);
				var target = file_text_read_real(file);
				file_text_close(file);
				SlideTransiton(trans_mode.goto, target);
			}
		}		
		break;
		case 1: SlideTransiton(trans_mode.goto, rm_options); break;
		case 0: game_end(); break;
	}	
}




gui_height = display_get_gui_height();
gui_width = display_get_gui_width();
gui_margin = 32;
menu_x = gui_width - 100;
menu_y = gui_height - gui_margin;
menu_x_target = gui_width - gui_margin;
menu_font = f_menu;
menu_itemheight = font_get_size(f_menu);
menu_committed = -1;
menu_control = true;

menu[4] = "Sound";
menu[3] = "placeholder";
menu[2] = "placeholder";
menu[1] = "placeholder";
menu[0] = "placeholder";


menu_items = array_length_1d(menu);
menu_cursor = 2;

{
    "id": "d8bc57d6-6241-4421-a223-c08ea2cd578e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objAmmo",
    "eventList": [
        {
            "id": "17a22144-7438-4630-b9d7-77655c3cd157",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b73fd4ae-4b7a-463d-9b3c-64a7dcefacd8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d8bc57d6-6241-4421-a223-c08ea2cd578e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4ca69066-adf0-4750-99c4-1f415ea913d9",
    "visible": true
}
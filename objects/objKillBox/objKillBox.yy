{
    "id": "62be7d83-8e5b-43b3-839a-8335fadd6e2e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objKillBox",
    "eventList": [
        {
            "id": "97190659-7902-4543-9997-5464a7782d6a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "62be7d83-8e5b-43b3-839a-8335fadd6e2e"
        },
        {
            "id": "a3d8f260-f61d-4316-bbd1-9138a30e64f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b73fd4ae-4b7a-463d-9b3c-64a7dcefacd8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "62be7d83-8e5b-43b3-839a-8335fadd6e2e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "94f6082c-2d72-4619-8309-ee5030912a30",
    "visible": true
}
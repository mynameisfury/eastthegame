{
    "id": "5cd62f58-136d-4144-8acc-3452cf2c2a31",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_checkpoint",
    "eventList": [
        {
            "id": "b9fac4cf-8c98-4743-82d2-09e9cd741c2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5cd62f58-136d-4144-8acc-3452cf2c2a31"
        },
        {
            "id": "154af91d-87f2-4050-8fd7-93402f205a9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5cd62f58-136d-4144-8acc-3452cf2c2a31"
        },
        {
            "id": "19701c4d-3eb9-4da5-b6f2-214c60cb5503",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "5cd62f58-136d-4144-8acc-3452cf2c2a31"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "462b3aa9-0979-45b7-9e40-c387d9d87fec",
    "visible": true
}
{
    "id": "601f9056-e638-4889-99ea-eb36b69f1863",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objWeaponPickup",
    "eventList": [
        {
            "id": "0655aafe-5ad0-4419-8d62-9987cc679784",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b73fd4ae-4b7a-463d-9b3c-64a7dcefacd8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "601f9056-e638-4889-99ea-eb36b69f1863"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
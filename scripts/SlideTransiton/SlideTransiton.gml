/// slidetransition(mode,targetroom)
/// @arg mode sets transition mode between next, restart and goto
/// @arg target sets room when uing goto mode

with (obj_transition)
{
	mode = argument[0];
	if (argument_count > 1) target = argument[1];
}

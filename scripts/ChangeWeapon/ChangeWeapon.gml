weapon = argument0


var wp_map = weapons[weapon]

sprite = wp_map[? "sprite"]
recoil = wp_map[? "recoil"]
damage = wp_map[? "damage"]
radius = wp_map[? "radius"]
bulletspeed = wp_map[? "bulletspeed"]
startup = wp_map[? "startup"]
bleed = wp_map[? "bleed"]
length = wp_map[? "length"]
firingdelay = wp_map[? "firingdelay"]
automatic = wp_map[? "automatic"]
